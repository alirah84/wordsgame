package com.ar.wordsgame;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.LevelViewHolder> {
    private List<Level> levels = new ArrayList<>();
    private OnRecyclerViewItemClickListener<Level> onRecyclerViewItemClickListener;

    public LevelAdapter(List<Level> levels,OnRecyclerViewItemClickListener<Level> onRecyclerViewItemClickListener) {
        this.levels = levels;
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }
    @NonNull
    @Override
    public LevelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_level,parent,false);
        return new LevelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LevelViewHolder holder, int position) {
        holder.bindLevels(levels.get(position));

    }

    @Override
    public int getItemCount() {
        return levels.size();
    }

    public class LevelViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;

        public LevelViewHolder(@NonNull View itemView) {
            super(itemView);
           textView = itemView.findViewById(R.id.tv_level_num);
        }
        public void bindLevels(final Level level){
            textView.setText(String.valueOf(level.getId()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerViewItemClickListener.onItemClick(level,getAdapterPosition());
                }
            });
        }
    }
}
