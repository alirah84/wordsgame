package com.ar.wordsgame;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GamePlayUtils {
    public static List<Level> createLevel(){
        List<Level> levels = new ArrayList<>();
        Level level1 = new Level();
        level1.setId(1);
        level1.getWords().add("آش");
        level1.getWords().add("آتش");
        level1.getWords().add("آرا");
        levels.add(level1);

        Level level2 = new Level();
        level2.setId(2);
        level2.getWords().add("کاخ");
        level2.getWords().add("خاک");

        levels.add(level2);
        return levels;
    }

    public static List<Character> extractUniqueCharacters(List<String> words){
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i <words.size() ; i++) {
            for (int j = 0; j <words.get(i).length() ; j++) {
                if(!characters.contains(words.get(i).charAt(j))) {
                    characters.add(words.get(i).charAt(j));
//                    if (words.get(i).charAt(j) == 'آ') {
//                        characters.set(characters.size() - 1, 'ا');
//                    }
                }
            }
        }

        return  characters;
    }
}
