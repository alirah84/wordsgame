package com.ar.wordsgame;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LevelFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_level,container,false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView levelRv = (RecyclerView) view;
        levelRv.setLayoutManager(new GridLayoutManager(getContext(),3));
        LevelAdapter levelAdapter = new LevelAdapter(GamePlayUtils.createLevel(), new OnRecyclerViewItemClickListener<Level>() {
            @Override
            public void onItemClick(Level level, int position) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("level",level);
                Navigation.findNavController(view).navigate(R.id.action_levelFragment_to_gameFragment,bundle);
            }
        });
        levelRv.setAdapter(levelAdapter);
    }
}
