package com.ar.wordsgame;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {
    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener<CharacterPlaceHolder> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    private OnRecyclerViewItemClickListener<CharacterPlaceHolder> onRecyclerViewItemClickListener;
    private List<CharacterPlaceHolder> characterPlaceHolders = new ArrayList<>();
    private boolean mustNotShowAculad;
    public CharacterAdapter(List<CharacterPlaceHolder> characterPlaceHolders,boolean mustNotShowAculad) {
        this.characterPlaceHolders = characterPlaceHolders;
        this.mustNotShowAculad = mustNotShowAculad;
    }

    public CharacterAdapter(boolean mustNotShowAculad){
        this.mustNotShowAculad = mustNotShowAculad;
    }

    public void add(CharacterPlaceHolder characterPlaceHolder){
        characterPlaceHolders.add(characterPlaceHolder);
        notifyItemInserted(characterPlaceHolders.size()-1);
    }

    public void clear(){
        characterPlaceHolders.clear();
        notifyDataSetChanged();
    }

    public String getWord(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < characterPlaceHolders.size(); i++) {
            stringBuilder.append(characterPlaceHolders.get(i).getCharacter());
        }
        return stringBuilder.toString();
    }

    public void makeCharacterVisible(String word){
        for (int i = 0; i < characterPlaceHolders.size(); i++) {
            if(characterPlaceHolders.get(i).getTag()!=null && characterPlaceHolders.get(i).getTag().equalsIgnoreCase(word)) {
                characterPlaceHolders.get(i).setVisible(true);
                notifyItemChanged(i);
            }
        }
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CharacterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_char,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        holder.bindCharacters(characterPlaceHolders.get(position));
    }

    @Override
    public int getItemCount() {
        return characterPlaceHolders.size();
    }

    public class CharacterViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        public CharacterViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_char);
        }

        public void bindCharacters(final CharacterPlaceHolder characterPlaceHolder) {
            if (characterPlaceHolder.isVisible()) {
                textView.setText(characterPlaceHolder.getCharacter().toString());
                if(mustNotShowAculad){
                    if(characterPlaceHolder.getCharacter().toString().equals("آ")){
                        textView.setText("ا");
                    }
                }
                textView.setVisibility(View.VISIBLE);
            }
            else textView.setVisibility(View.INVISIBLE);

            if(characterPlaceHolder.isNull()){
                itemView.setBackground(null);
            }
            else {
                itemView.setBackgroundResource(R.drawable.background_char);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onRecyclerViewItemClickListener!=null){
                        onRecyclerViewItemClickListener.onItemClick(characterPlaceHolder,getAdapterPosition());
                    }
                }
            });

        }
    }
}
