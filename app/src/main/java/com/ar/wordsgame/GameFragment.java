package com.ar.wordsgame;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GameFragment extends Fragment {
    private Level level;
    private View guessActionContainer;
    private CharacterAdapter guessAdapter;
    private CharacterAdapter wordsAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        level = getArguments().getParcelable("level");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_game,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
      guessActionContainer = view.findViewById(R.id.frame_game_guessActionContainer);
        super.onViewCreated(view, savedInstanceState);
        List<Character> uniqueCharacters = GamePlayUtils.extractUniqueCharacters(level.getWords());
        List<CharacterPlaceHolder> characterPlaceHolders = new ArrayList<>();
      RecyclerView gameRecyclerView = view.findViewById(R.id.rv_game_characters);
      gameRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        for (int i = 0; i <uniqueCharacters.size() ; i++) {
            CharacterPlaceHolder characterPlaceHolder = new CharacterPlaceHolder();
            characterPlaceHolder.setVisible(true);
            characterPlaceHolder.setCharacter(uniqueCharacters.get(i));
            characterPlaceHolders.add(characterPlaceHolder);
        }

        CharacterAdapter characterAdapter = new CharacterAdapter(characterPlaceHolders,true);
        gameRecyclerView.setAdapter(characterAdapter);
        characterAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<CharacterPlaceHolder>() {
            @Override
            public void onItemClick(CharacterPlaceHolder item, int position) {
                guessActionContainer.setVisibility(View.VISIBLE);
                guessAdapter.add(item);
            }
        });

      RecyclerView guessRv = view.findViewById(R.id.rv_game_guess);
      guessRv.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
      guessAdapter = new CharacterAdapter(true);
      guessRv.setAdapter(guessAdapter);

      final View cancelBtn = view.findViewById(R.id.btn_game_cancel);
      cancelBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              guessActionContainer.setVisibility(View.GONE);
              guessAdapter.clear();
          }
      });

      View acceptBtn = view.findViewById(R.id.btn_game_accept);
      acceptBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
             String wordGuessed = guessAdapter.getWord();
              for (int i = 0; i < level.getWords().size(); i++) {
                  if (wordGuessed.equalsIgnoreCase(level.getWords().get(i))) {
                      Toast.makeText(getContext(), " کلمه درست حدس زده شد " + wordGuessed, Toast.LENGTH_SHORT).show();
                      cancelBtn.performClick();
                      wordsAdapter.makeCharacterVisible(wordGuessed);
                      return;
                  }
              }
              Toast.makeText(getContext(), "صحیح نیست", Toast.LENGTH_SHORT).show();
              cancelBtn.performClick();
          }
      });

      int maxLenght = 0;
        for (int i = 0; i < level.getWords().size(); i++) {
            if (level.getWords().get(i).length()>maxLenght)
                maxLenght = level.getWords().get(i).length();
        }

      RecyclerView wordsRv = view.findViewById(R.id.rv_game_words);
      wordsRv.setLayoutManager(new GridLayoutManager(getContext(),maxLenght));
      List<CharacterPlaceHolder> wordsCharacter = new ArrayList<>();
        for (int i = 0; i <level.getWords().size() ; i++) {
            for (int j = 0; j <maxLenght ; j++) {
                CharacterPlaceHolder characterPlaceHolder = new CharacterPlaceHolder();
                if(j<level.getWords().get(i).length()){
                    characterPlaceHolder.setCharacter(level.getWords().get(i).charAt(j));
                    characterPlaceHolder.setVisible(false);
                    characterPlaceHolder.setNull(false);
                    characterPlaceHolder.setTag(level.getWords().get(i));
                }
                else{
                    characterPlaceHolder.setNull(true);
                }
                wordsCharacter.add(characterPlaceHolder);
            }
        }
        wordsAdapter = new CharacterAdapter(wordsCharacter,false);
        wordsRv.setAdapter(wordsAdapter);
    }
}
