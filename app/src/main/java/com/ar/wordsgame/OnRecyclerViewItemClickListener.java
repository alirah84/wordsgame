package com.ar.wordsgame;

public interface OnRecyclerViewItemClickListener<T> {
    void onItemClick(T item,int position);
}
